import React, { Component } from 'react';

export class AppFooter extends Component {

    render() {
        return  (
            <div className="layout-footer">
                <span className="footer-text" style={{'marginRight': '5px'}}>@Copyright</span>
                <img src="assets/layout/images/Logo_SPECTRAL.png" alt="" width="80"/>
                <span className="footer-text" style={{'marginLeft': '5px'}}>SSE GROUP</span>
            </div>
        );
    }
}