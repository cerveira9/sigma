import React, { Component } from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { AutoComplete } from 'primereact/autocomplete';
import {Password} from 'primereact/password';

class Login extends Component {

    constructor() {
        super();
    }

    render() {

        const header = <img alt="Logo" src='assets/layout/images/sse.png' />;
        const footer = <span>
            <Button label="Entrar" icon="pi pi-check" style={{ marginTop: '1em' }} />
        </span>;
        return (
            <div style={
                styles.container
            }>
                <Card style={
                    styles.boxImage
                } footer={footer}>
                    {header}
                    <br />
                    <div style={styles.box}>
                        <h3>Usuário</h3>
                        <AutoComplete />
                        <h3>Senha</h3>
                        <Password />
                    </div>
                </Card>
            </div>
        );
    }
}

const styles = {
    container: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        marginTop: "60px"
    },
    box: {
        textAlign: 'center'
    },
    boxImage: {
        width: '300px',
        textAlign: 'center'
    }

}


export default Login;