import React, { Component } from 'react';
import { Card } from 'primereact/card';
import { AutoComplete } from 'primereact/autocomplete';
import {Dropdown} from 'primereact/dropdown';
import {Button} from 'primereact/button';

class CadastroMangueiras extends Component {

    render() {
        return (
            <div>
                <div style={{
                    display: 'flex',
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: "30px"
                }}>
                    <Card title="Cadastro Mangueira" style={{ width: '360px' }}>
                        <div>
                            <h3>Campo1</h3>
                            <AutoComplete />
                            <h3>Campo2</h3>
                            <Dropdown />
                            <h3>Campo3</h3>
                            <AutoComplete />
                            <h3>Campo4</h3>
                            <AutoComplete />
                            <br/>
                            <Button style={{marginTop: "30px"}} label="Cadastrar" />
                        </div>
                    </Card>
                </div>

            </div>
        );
    }
}

export default CadastroMangueiras;