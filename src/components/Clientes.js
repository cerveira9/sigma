import React, { Component } from 'react';
import { Button } from 'primereact/button';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {ColumnGroup} from 'primereact/columngroup';
import {Row} from 'primereact/row';

class Clientes extends Component {

    constructor() {
        super();
        this.state = {
            sales: [
                { brand: 'A', lastYearSale: '51%', thisYearSale: '40%', lastYearProfit: 'R$54,406.00', thisYearProfit: 'R$43,342' },
                { brand: 'B', lastYearSale: '83%', thisYearSale: '96%', lastYearProfit: 'R$423,132', thisYearProfit: 'R$312,122' },
                { brand: 'C', lastYearSale: '38%', thisYearSale: '5%', lastYearProfit: 'R$12,321', thisYearProfit: 'R$8,500' },
                { brand: 'D', lastYearSale: '49%', thisYearSale: '22%', lastYearProfit: 'R$745,232', thisYearProfit: 'R$650,323,' },
                { brand: 'E', lastYearSale: '17%', thisYearSale: '79%', lastYearProfit: 'R$643,242', thisYearProfit: 'R$500,332' },
                { brand: 'F', lastYearSale: '52%', thisYearSale: ' 65%', lastYearProfit: 'R$421,132', thisYearProfit: 'R$150,005' },
                { brand: 'G', lastYearSale: '82%', thisYearSale: '12%', lastYearProfit: 'R$131,211', thisYearProfit: 'R$100,214' },
                { brand: 'H', lastYearSale: '44%', thisYearSale: '45%', lastYearProfit: 'R$66,442', thisYearProfit: 'R$53,322' },
                { brand: 'I', lastYearSale: '90%', thisYearSale: '56%', lastYearProfit: 'R$765,442', thisYearProfit: 'R$296,232' },
                { brand: 'J', lastYearSale: '75%', thisYearSale: '54%', lastYearProfit: 'R$21,212', thisYearProfit: 'R$12,533' }
            ]
        };
    }

    render() {
        let headerGroup = <ColumnGroup>
            <Row>
                <Column header="Cliente" rowSpan={3} />
                <Column header="Taxa de Pedidos" colSpan={4} />
            </Row>
            <Row>
                <Column header="Vendas" colSpan={2} />
                <Column header="Lucros" colSpan={2} />
            </Row>
            <Row>
                <Column header="Ultimo Ano" />
                <Column header="Esse Ano" />
                <Column header="Ultimo Ano" />
                <Column header="Esse Ano" />
            </Row>
        </ColumnGroup>;

        let footerGroup = <ColumnGroup>
            <Row>
                <Column footer="Total:" colSpan={3} />
                <Column footer="R$506,202" />
                <Column footer="R$531,020" />
            </Row>
        </ColumnGroup>;
        return (
            <div>
                <h1 style={{ marginBottom: "30px" }}>CLIENTES</h1>
                <Button style={{ marginBottom: "30px" }} label="Cadastrar Cliente" />
                <div style={{ textAlign: "center" }}> <DataTable value={this.state.sales} headerColumnGroup={headerGroup} footerColumnGroup={footerGroup}>
                    <Column field="brand" />
                    <Column field="lastYearSale" />
                    <Column field="thisYearSale" />
                    <Column field="lastYearProfit" />
                    <Column field="thisYearProfit" />
                </DataTable></div>
            </div>
        );

    }
}

export default Clientes;