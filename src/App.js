import React, { Component } from 'react';
import classNames from 'classnames';
import { AppTopbar } from './AppTopbar';
import { AppFooter } from './AppFooter';
import { AppMenu } from './AppMenu';
import { AppProfile } from './AppProfile';
import { Route } from 'react-router-dom';
import { Dashboard } from './components/Dashboard';
import { FormsDemo } from './components/FormsDemo';
import { SampleDemo } from './components/SampleDemo';
import { DataDemo } from './components/DataDemo';
import { PanelsDemo } from './components/PanelsDemo';
import { OverlaysDemo } from './components/OverlaysDemo';
import { MenusDemo } from './components/MenusDemo';
import { MessagesDemo } from './components/MessagesDemo';
import { ChartsDemo } from './components/ChartsDemo';
import { MiscDemo } from './components/MiscDemo';
import { EmptyPage } from './components/EmptyPage';
import { Documentation } from "./components/Documentation";
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import './layout/layout.scss';
import './App.scss';
import Mangueiras from './components/Mangueiras';
import Fornecedores from './components/Fornecedores';
import Clientes from './components/Clientes';
import Usuarios from './components/Usuarios';
import CadastroMangueiras from './components/CadastroMangueiras';
import Login from './components/Login';

class App extends Component {

    constructor() {
        super();
        this.state = {
            layoutMode: 'static',
            layoutColorMode: 'light',
            staticMenuInactive: false,
            overlayMenuActive: false,
            mobileMenuActive: false,
            log: true,
        };

        this.onWrapperClick = this.onWrapperClick.bind(this);
        this.onToggleMenu = this.onToggleMenu.bind(this);
        this.onSidebarClick = this.onSidebarClick.bind(this);
        this.onMenuItemClick = this.onMenuItemClick.bind(this);
        this.createMenu();
    }

    onWrapperClick(event) {
        if (!this.menuClick) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            });
        }

        this.menuClick = false;
    }

    onToggleMenu(event) {
        this.menuClick = true;

        if (this.isDesktop()) {
            if (this.state.layoutMode === 'overlay') {
                this.setState({
                    overlayMenuActive: !this.state.overlayMenuActive
                });
            }
            else if (this.state.layoutMode === 'static') {
                this.setState({
                    staticMenuInactive: !this.state.staticMenuInactive
                });
            }
        }
        else {
            const mobileMenuActive = this.state.mobileMenuActive;
            this.setState({
                mobileMenuActive: !mobileMenuActive
            });
        }

        event.preventDefault();
    }

    onSidebarClick(event) {
        this.menuClick = true;
    }

    onMenuItemClick(event) {
        if (!event.item.items) {
            this.setState({
                overlayMenuActive: false,
                mobileMenuActive: false
            })
        }
    }

    createMenu() {
        this.menu = [
            { label: 'Dashboard', icon: 'pi pi-fw pi-home', command: () => { window.location = '/' } },
            {
                label: 'Mangueiras', icon: 'pi pi-fw pi-cog', command: () => { window.location = '#/mangueiras' },
            },
            {
                label: 'Fornecedores', icon: 'pi pi-fw pi-align-left', command: () => { window.location = '#/fornecedores' }
            },
            {
                label: 'Clientes', icon: 'pi pi-fw pi-globe', command: () => { window.location = '#/clientes' }
            },
            {
                label: 'Usuários', icon: 'pi pi-fw pi-user', command: () => { window.location = '#/usuarios' }
            },
            {
                label: 'Relatórios', icon: 'pi pi-fw pi-file', command: () => { window.location = '#/relatorios' }
                // items: [
                //     {
                //         label: 'Submenu 1', icon: 'pi pi-fw pi-bookmark',
                //         items: [
                //             {
                //                 label: 'Submenu 1.1', icon: 'pi pi-fw pi-bookmark',
                //                 items: [
                //                     {label: 'Submenu 1.1.1', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 1.1.2', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 1.1.3', icon: 'pi pi-fw pi-bookmark'},
                //                 ]
                //             },
                //             {
                //                 label: 'Submenu 1.2', icon: 'pi pi-fw pi-bookmark',
                //                 items: [
                //                     {label: 'Submenu 1.2.1', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 1.2.2', icon: 'pi pi-fw pi-bookmark'}
                //                 ]
                //             },
                //         ]
                //     },
                //     {
                //         label: 'Submenu 2', icon: 'pi pi-fw pi-bookmark',
                //         items: [
                //             {
                //                 label: 'Submenu 2.1', icon: 'pi pi-fw pi-bookmark',
                //                 items: [
                //                     {label: 'Submenu 2.1.1', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 2.1.2', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 2.1.3', icon: 'pi pi-fw pi-bookmark'},
                //                 ]
                //             },
                //             {
                //                 label: 'Submenu 2.2', icon: 'pi pi-fw pi-bookmark',
                //                 items: [
                //                     {label: 'Submenu 2.2.1', icon: 'pi pi-fw pi-bookmark'},
                //                     {label: 'Submenu 2.2.2', icon: 'pi pi-fw pi-bookmark'}
                //                 ]
                //             }
                //         ]
                //     }
                // ]
            },
        ];
    }

    addClass(element, className) {
        if (element.classList)
            element.classList.add(className);
        else
            element.className += ' ' + className;
    }

    removeClass(element, className) {
        if (element.classList)
            element.classList.remove(className);
        else
            element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }

    isDesktop() {
        return window.innerWidth > 1024;
    }

    componentDidUpdate() {
        if (this.state.mobileMenuActive)
            this.addClass(document.body, 'body-overflow-hidden');
        else
            this.removeClass(document.body, 'body-overflow-hidden');
    }

    render() {
        const logo = this.state.layoutColorMode === 'dark' ? 'assets/layout/images/logo-white.svg' : 'assets/layout/images/logo.svg';

        const wrapperClass = classNames('layout-wrapper', {
            'layout-overlay': this.state.layoutMode === 'overlay',
            'layout-static': this.state.layoutMode === 'static',
            'layout-static-sidebar-inactive': this.state.staticMenuInactive && this.state.layoutMode === 'static',
            'layout-overlay-sidebar-active': this.state.overlayMenuActive && this.state.layoutMode === 'overlay',
            'layout-mobile-sidebar-active': this.state.mobileMenuActive
        });

        const sidebarClassName = classNames("layout-sidebar", {
            'layout-sidebar-dark': this.state.layoutColorMode === 'dark',
            'layout-sidebar-light': this.state.layoutColorMode === 'light'
        });
        if (this.state.log) {
            return (

                <div className={wrapperClass} onClick={this.onWrapperClick}>
                    <AppTopbar onToggleMenu={this.onToggleMenu} />

                    <div ref={(el) => this.sidebar = el} className={sidebarClassName} onClick={this.onSidebarClick}>
                        <div className="layout-logo">
                            <img alt="Logo" src="assets/layout/images/sse.png" />
                        </div>
                        <AppProfile />
                        <AppMenu model={this.menu} onMenuItemClick={this.onMenuItemClick} />
                    </div>

                    <div className="layout-main">
                        <Route path="/" exact component={Dashboard} />
                        <Route path="/mangueiras" component={Mangueiras} />
                        <Route path="/fornecedores" component={Fornecedores} />
                        <Route path="/clientes" component={Clientes} />
                        <Route path="/usuarios" component={Usuarios} />
                        <Route path="/cadastromangueiras" component={CadastroMangueiras} />
                        <Route path="/login" component={Login} />
                        <Route path="/messages" component={MessagesDemo} />
                        <Route path="/charts" component={ChartsDemo} />
                        <Route path="/misc" component={MiscDemo} />
                        <Route path="/empty" component={EmptyPage} />
                        <Route path="/documentation" component={Documentation} />
                    </div>


                    <div className="layout-mask"></div>
                </div >
            );
        } else {
            return(
                <Login />
            );
        }

    }
}

export default App;
